let http = require("http");

http.createServer(function(req, res) {
	if (req.url == "/items" && req.method == "GET") {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Data retrieved from database');
	}
}).listen(4000);

console.log('Server is running at localhost:4000');